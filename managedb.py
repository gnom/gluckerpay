#!/usr/bin/env python
from app import db
from config import SQLALCHEMY_DATABASE_URI
from config import SQLALCHEMY_MIGRATE_REPO
from migrate.versioning import api
import os.path
import imp
import sys
from app.models import User
from getpass import getpass


def db_insert_admin():
    while True:
        print('Create initial user with administrative privileges')
        username = input('username: ')
        print('You have to remember the password otherwise you can not login!')
        password = getpass('password: ')
        print('Repeat the password to make sure there\'s no typo.')
        password2 = getpass('password: ')
        if password == password2:
            break
        else:
            print('\nERROR - The passwords didn\'t match. Try again!')
            continue
    u = User(user=username.strip(), role_id=1, password=password.strip())
    db.session.add(u)
    db.session.commit()
    print('User \'%s\' created with administrative privileges.' % username.strip())


def db_create():
    print('Setting up migration repository...')
    db.create_all()
    if not os.path.exists(SQLALCHEMY_MIGRATE_REPO):
        api.create(SQLALCHEMY_MIGRATE_REPO, 'database repository')
        api.version_control(SQLALCHEMY_DATABASE_URI, SQLALCHEMY_MIGRATE_REPO)
        print('New SQLAlchemy migration repository created. Path: %s' % SQLALCHEMY_MIGRATE_REPO)
        print('Setup completed')
    else:
        print('ERROR - SQLAlchemy migration repository already exists. Path: %s' % SQLALCHEMY_MIGRATE_REPO)
        print('Current version of migration: %d' % api.version(SQLALCHEMY_MIGRATE_REPO))
        print()
        sys.exit(1)


def db_migrate():
    print('Creating / Upgrading database...')
    v = api.db_version(SQLALCHEMY_DATABASE_URI, SQLALCHEMY_MIGRATE_REPO)
    print('Updating from version %d -> %d' % (v, v + 1))
    migration = SQLALCHEMY_MIGRATE_REPO + ('/versions/%03d_migration.py' % (v + 1))
    tmp_module = imp.new_module('old_model')
    old_model = api.create_model(SQLALCHEMY_DATABASE_URI, SQLALCHEMY_MIGRATE_REPO)
    exec(old_model, tmp_module.__dict__)
    print('Collecting information to generate migration script')
    script = api.make_update_script_for_model(SQLALCHEMY_DATABASE_URI, SQLALCHEMY_MIGRATE_REPO, tmp_module.meta,
                                              db.metadata)
    open(migration, "wt").write(script)
    print('Migration script created')
    api.upgrade(SQLALCHEMY_DATABASE_URI, SQLALCHEMY_MIGRATE_REPO)
    v = api.db_version(SQLALCHEMY_DATABASE_URI, SQLALCHEMY_MIGRATE_REPO)
    print('New migration saved as %s' % migration)
    print('Current database version: %s' % str(v))


if __name__ == '__main__':
    if sys.argv[1] == 'create':
        db_create()
        print()
        db_migrate()
        print()
        db_insert_admin()
        print()
        sys.exit(0)

    if sys.argv[1] == 'migrate':
        db_migrate()
        sys.exit(0)

    print('Implement some instructions on how to use, here!')
    # TODO: Implement a brief set of instructions and switch to argparses