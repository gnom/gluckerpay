from app import db, login_manager, app, log
from flask import flash
from flask_login import UserMixin, AnonymousUserMixin
from werkzeug.security import generate_password_hash, check_password_hash
from sqlalchemy.sql import func
import decimal
import uuid
import os


class Permission():
    """
    Permissions for MatePay.
    GUESTs are limited to a prepaid account
    MEMBERs may have credit up to a configurable limit
    PREPAID users are anonymous and may only use their debit
    """
    PREPAID = 0x01
    GUEST = 0x02
    MEMBER = 0x04
    ADMIN = 0x80


class Product(db.Model):
    __tablename__ = 'products'
    id = db.Column(db.Integer, primary_key=True)
    product = db.Column(db.String(64), index=True, unique=True)
    price = db.Column(db.Numeric(precision=2))
    quantity = db.Column(db.Numeric(precision=2))
    image = db.Column(db.String(64), default=None)
    active = db.Column(db.Boolean, index=True, default=False)
    stock = db.Column(db.Integer, index=True, default=0)
    ppc = db.Column(db.Integer)
    ean = db.Column(db.String(13), default=None)
    id_categories = db.Column(db.Integer, db.ForeignKey('categories.id'))
    id_packages = db.Column(db.Integer, db.ForeignKey('packages.id'))
    transact = db.relationship('Transaction', backref='transProd', lazy='dynamic')

    def __repr__(self):
        return '<Product %r>' % self.product

    def sell(self, amount=1):
        self.stock = self.stock - amount
        db.session.add(self)

    def add_to_stock(self, amount):
        self.stock = self.stock + amount
        db.session.add(self)


class Category(db.Model):
    __tablename__ = 'categories'
    id = db.Column(db.Integer, primary_key=True)
    category = db.Column(db.String(64), index=True, unique=True)
    products = db.relationship('Product', backref='productCategory', lazy='dynamic')

    def __repr__(self):
        return '<Category %r>' % self.category


class Package(db.Model):
    __tablename__ = 'packages'
    id = db.Column(db.Integer, primary_key=True)
    package = db.Column(db.String(32), unique=True, index=True)
    unit = db.Column(db.String(32))
    unit_abbr = db.Column(db.String(6))
    products = db.relationship('Product', backref='productPackage', lazy='dynamic')

    def __repr__(self):
        return '<Package %r>' % self.package


class User(UserMixin, db.Model):
    __tablename__ = 'users'
    id = db.Column(db.Integer, primary_key=True)
    user = db.Column(db.String(64), unique=True, index=True)
    passwd = db.Column(db.String(128), default=None)
    balance = db.Column(db.Numeric(precision=2), default=0)
    avatar = db.Column(db.String(64))
    last = db.Column(db.DateTime, server_default=func.now())
    role_id = db.Column(db.Integer, db.ForeignKey('roles.id'))
    transactions = db.relationship('Transaction', backref='transUser', lazy='dynamic')
    ean = db.Column(db.String(13), default=None, unique=True)
    def __repr__(self):
        return '<User %r>' % self.user

    @property
    def password(self):
        return False

    @password.setter
    def password(self, password):
        self.passwd = generate_password_hash(password)

    def verify_password(self, password):
        return check_password_hash(self.passwd, password)

    def can(self, permissions):
        return self.role is not None and (self.role.permissions & permissions) == permissions

    def is_admin(self):
        return True
        #return self.can(Permission.ADMIN)

    def buy_product(self, pid):
        product = Product.query.filter_by(id=pid).first()
        self.balance = self.balance - product.price
        product.stock -= 1
        db.session.commit()
        trans = Transaction()
        trans.make(self.id, pid)
        return True

    def charge_account(self, sum=None):
        try:
            sum = float(sum)
        except:
            flash('Es wurde ein ungültiger Betrag angegeben')
            return False
        if not sum in app.config['RECHARGE_VALUES']:
            flash('Es wurde ein ungültiger Betrag angegeben')
            return False
        self.balance = self.balance + decimal.Decimal(sum)
        try:
            db.session.commit()
        except:
            flash('Der Aufladervorgang wurde fehlerhaft beendet. Kontaktiere bitte einen Admin!')
            return False
        flash('Konto erfolgreich aufgeladen!')
        return True

    def set_avatar(self, avatar=False):
        if not avatar:
            return False
        imgpath = None
        while imgpath == None or os.path.isfile(imgpath):
            imgname = str(uuid.uuid4()) + '.png'
            imgpath = os.path.join(app.config['BASEDIR'], imgname)
            print(imgpath)
        return True

    def assign_card(self, code):
        try:
            int(code)
        except:
            flash('Der Barcode darf nur aus Ziffern bestehen')
            log.warning('Barcodes may only consist of numbers, but %s was given.' % str(code))
            return False

        if str(code).len() != 13:
            flash('Der Barcode muss 13 Stellen lang sein')
            log.warning('Barcodes have to be 13 digits long, but %s was given.' % str(code))
            return False

        self.ean = code
        try:
            db.session.add(self)
        except:
            flash('Der Barcode %i ist bereits in verwendung!' % code)
            log.warning('This barcode %i is already in use.' % code)

        return True

class AnonymousUser(AnonymousUserMixin):
    def can(self, permissions):
        return False

    def is_admin(self):
        return False


login_manager.anonymous_user = AnonymousUser


@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))


class Role(db.Model):
    __tablename__ = 'roles'
    id = db.Column(db.Integer, primary_key=True)
    role = db.Column(db.String(32), unique=True)
    default = db.Column(db.Boolean, default=False, index=True)
    permissions = db.Column(db.Integer)
    users = db.relationship('User', backref='role', lazy='dynamic')

    def __repr__(self):
        return '<Role %r>' % self.role


class Transaction(db.Model):
    __tablename__ = 'transactions'
    id = db.Column(db.Integer, primary_key=True)
    ts = db.Column(db.DateTime, server_default=func.now())
    user_user = db.Column(db.String(64))
    product_product = db.Column(db.String(64))
    product_price = db.Column(db.String(30))
    id_user = db.Column(db.Integer, db.ForeignKey('users.id'))
    id_product = db.Column(db.Integer,db.ForeignKey('products.id'))

    def __repr__(self):
        return '<Transaction %r: %r -> %r>' % (self.ts, self.user_user, self.id_product)

    def make(self, uid, pid):
        prod = Product.query.filter_by(id=pid).first()
        user = User.query.filter_by(id=uid).first()
        self.ts = func.now()
        self.user_user = str(user.user)
        self.product_product = str(prod.product)
        self.product_price = str(prod.price)
        self.id_user = user.id
        self.id_product = prod.id
        db.session.add(self)
        db.session.commit()
        return True
