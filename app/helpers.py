from app import db, log
import random

class ean():
    prefix = 230
    mancodelen = 4
    prodcodelen = 5


    def check_13(self, barcode: int, checksum: int = False):
        try:
            int(barcode)
            int(checksum)
        except ValueError as err:
            log.error(err)

        if (not str(barcode).len() == 13 and not checksum) or
            (not str(barcode).len() == 12 and str(checksum).len() != 1):
            log.error('wrong length of ean.check_13 parameters')
            return False


    def generate_user(self, tobc='ean13'):
        code = prefix
        for x in range(1,5):
            code = code + str(random.randrange(0, 9))

        print(code)
        return code
