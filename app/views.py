from flask import render_template, redirect, flash, request, url_for
from app import app
from .forms import *
from flask_login import login_required, login_user, current_user, logout_user
from .models import Product, User, Category
from . import db, log
import decimal


@app.template_filter('currency')
def filter_decimal(value):
    if value == None:
        value = 0
    value = decimal.Decimal((value))
    value = str(value.quantize(decimal.Decimal('1.00')))
    value = value.replace('.', app.config['DECIMAL_SEPARATOR'])
    if app.config['CURRENCY_PLACEMENT'] == 'after':
        value = '%s %s' % (value, app.config['CURRENCY_SIGN'])
    else:
        value = '% %s' % (app.config['CURRENCY_SIGN'], value)
    return value


@app.template_filter('decimal')
def reverse_filter(value):
    return str(('%0.2f' % float(value))).replace('.', ',')


@app.route('/')
@app.route('/index')
def index():
    users = User.query.all()
    return render_template('index.html', users=users)


@app.route('/login', methods=['GET', 'POST'])
def login():
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(user=form.username.data).first()
        if user is not None and user.verify_password(form.password.data):
            login_user(user, False)
            return redirect(request.args.get('next') or url_for('index'))
        flash('Das Kombination aus Benutzername & Passwort stimmt nicht.')
    return render_template('login.html', form=form)


@app.route('/logout', methods=['GET', 'POST'])
@login_required
def logout():
    logout_user()
    flash('Du hast dich abgemeldet.')
    return redirect(url_for('index'))


@app.route('/user/<uid>/edit', methods=['GET', 'POST'])
def user_edit(uid):
    uid = int(uid)
    user = User.query.filter_by(id=uid).first()
    if not user:
        flash('Es gibt einen Benutzer mit der gewählten ID')
        return redirect(url_for('index'))
    form = UserEditForm()
    form.username.data = user.user
    form.id.data = user.id
    if user.balance == None:
        user.balance = 0
    balance = decimal.Decimal(user.balance)
    form.balance.data = str(balance.quantize(decimal.Decimal(('0.00')))).replace('.', ',')
    user.set_avatar('foobar')
    if True:
        for field in form:
            if field.name == 'id':
                continue
            field.render_kw = {'readonly': False}
    return render_template('user_edit.html', form=form)


@app.route('/user/<uid>', methods=['GET'])
def product_select(uid):
    uid = int(uid)
    user = User.query.filter_by(id=uid).first()
    products = Product.query.join(Package, Category).all()
    if not user:
        flash('Es gibt keinen Nutzer mit der gewählten ID')
        return redirect(url_for('index'))

    return render_template('product_select.html', user=user, products=products, charges=app.config['RECHARGE_VALUES'])


@app.route('/user/<uid>/buy/<pid>', methods=['GET'])
def user_buy_prod(uid, pid):
    flash('Benutzer %s kaufte Produkt %s' % (uid, pid))
    user = User.query.filter_by(id=int(uid)).first()
    user.buy_product(int(pid))
    return redirect(url_for('index'))


@app.route('/user/<uid>/charge/<sum>')
def user_charge(uid, sum):
    user = User.query.filter_by(id=int(uid)).first()
    user.charge_account(float(sum))
    return redirect(url_for('product_select', uid=uid))


@app.route('/admin', methods=['GET'])
@login_required
def admin():
    users = User.query.all()
    prods = Product.query.all()
    cats = Category.query.all()
    packs = Package.query.all()
    return render_template('admin.html', users=users, prods=prods, cats=cats, packs=packs)


@app.route('/user/add', methods=['POST', 'GET'])
@login_required
def user_add():
    form = UserAddForm()
    if form.validate_on_submit():
        user = User(user=form.username.data.strip(),
                    passwd=form.password.data.strip(),
                    role_id=form.role.data.strip(),
                    last=None,
                    balance=form.balance.data.strip().replace(',','.'),
                    avatar=None)
        db.session.add(user)
        db.session.commit()
        flash('Benutzer angelegt')
        return redirect(url_for('user_edit', uid=user.id))

    return render_template('user_add.html', form=form)


@app.route('/product/add', methods=['POST', 'GET'])
def product_add():
    form = ProductAddForm()
    form.category.choices = [(cat.id, cat.category) for cat in Category.query.order_by('category')]
    form.package.choices = [(pac.id, pac.package) for pac in Package.query.order_by('package')]
    if form.validate_on_submit():
        if form.active.data == 'false':
            active=False
        else:
            active=True
        prod = Product(product=form.prod.data.strip(),
                       price=form.price.data.strip().replace(',', '.'),
                       quantity=form.quantity.data.strip().replace(',', '.'),
                       image=None,
                       active=active,
                       stock=form.stock.data,
                       ppc=form.ppc.data,
                       id_packages=form.package.data,
                       id_categories=form.category.data)
        db.session.add(prod)
        db.session.commit()
        flash('Produkt gespeichert.')
        return redirect(url_for('admin'))

    return render_template('product_add.html', form=form)


@app.route('/category/add', methods=['POST', 'GET'])
def category_add():
    form = CategoryAddForm()
    if form.validate_on_submit():
        category = Category(category=form.cat.data.strip())
        db.session.add(category)
        db.session.commit()
        flash('Kategorie angelegt')
        return redirect(url_for('admin'))

    return render_template('category_add.html', form=form)


@app.route('/package/add', methods=['POST', 'GET'])
def package_add():
    form = PackageAddForm()
    if form.validate_on_submit():
        package = Package(package=form.packing.data.strip(),
                          unit=form.unit.data.strip(),
                          unit_abbr=form.unit_abbr.data.strip())
        db.session.add(package)
        db.session.commit()
        flash('Verpackung angelegt')
        return redirect(url_for('admin'))

    return render_template('package_add.html', form=form)

@app.route('/package/delete', methods=['GET', 'POST'])
def package_del():
    pass
