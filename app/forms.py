from flask_wtf import FlaskForm
from wtforms import StringField, TextAreaField, PasswordField, SubmitField, DateTimeField, SelectField, DecimalField, FileField, IntegerField, BooleanField
from wtforms.validators import  InputRequired, Regexp
from wtforms import ValidationError
from .models import User, Product, Category, Package
import decimal


class LoginForm(FlaskForm):
    username = StringField('Benutzername', validators=[InputRequired()])
    password = PasswordField('Passwort', validators=[InputRequired()])
    submit = SubmitField('Anmelden')


class UserAddForm(FlaskForm):
    username = StringField('Benutzername', validators=[InputRequired()])
    role = SelectField('Rolle', choices=[('1', 'Gast'), ('2', 'Mitglied'), ('3', 'Admin')])
    password = PasswordField('Passwort', description='Wird nur für Benutzer mit der Rolle "Admin" benötigt.')
    balance = StringField('Guthaben', validators=[InputRequired(), Regexp(r'^[-+]?(?<![\d.,])(\d{1,2}|\d{0,2}[.,]\d{1,2})?(?![\d.,])$', message='Das ist kein gültiges Zahlenformat. Versuche 0 oder 12,32')], default="0")
    submit = SubmitField('Benutzer anlegen')

    def validate_username(self, username):
        if User.query.filter_by(user=username.data).first():
            raise ValidationError('Benutzername wird bereits verwendet.')

    def validate_password(self, password):
        if self.role.data == '3' and len(password.data) < 8:
            raise ValidationError('Wenn die Rolle Admin gewählt ist, muss ein Passwort vergeben werden.')

class UserEditForm(FlaskForm):
    id = StringField('Nutzer-ID', render_kw={'readonly': True})
    username = StringField('Benutzername', render_kw={'readonly': True})
    balance = StringField('Guthaben', validators=[InputRequired(), Regexp(r'^(?<![\d.,])(\d{1,2}|\d{0,2}[.,]\d{1,2})?(?![\d.,])$', message='Das ist kein gültiges Zahlenformat. Versuche 0 oder 12,32')], render_kw={'readonly': True})
    avatar = FileField('Avatar')
    submit = SubmitField('Änderungen speichern')


class ProductAddForm(FlaskForm):
    prod = StringField('Produkt', validators=[InputRequired()])
    price = StringField('Preis', validators=[InputRequired(),  Regexp(r'^(?<![\d.,])(\d{1,2}|\d{0,2}[.,]\d{1,2})?(?![\d.,])$', message='Das ist kein gültiges Zahlenformat. Versuche 0 oder 12,32')])
    quantity = StringField('Menge', validators=[InputRequired()])
    package = SelectField('Verkaufseinheit', coerce=int)
    stock = IntegerField('Lagerbestand')
    ppc = IntegerField('Menge pro EKVP')
    category = SelectField('Kategorie', coerce=int)
    image = FileField('Produktbild')
    active = BooleanField('Aktiv?')
    submit = SubmitField('Produkt speichern')

    def validate_product(self, product):
        if Product.query.filter_by(product=product.prod).first():
            raise ValidationError('Produkt exisitiert bereits.')


class CategoryAddForm(FlaskForm):
    cat = StringField('Kategorie', validators=[InputRequired()])
    submit = SubmitField('Kategorie speichern')

    def verify_cat(self, category):
        if Category.query.filter_by(category=category.category).first():
            raise ValidationError('Kategorie existiert bereits.')


class PackageAddForm(FlaskForm):
    packing = StringField('Verpackung')
    unit = StringField('Einheit')
    unit_abbr = StringField('Abkürzung Einheit')
    submit = SubmitField('Verpackung speichern')

    def validate_package(self, package):
        if Package.query.filter_by(package=package.packing).first():
            raise ValidationError('Verpackung existiert bereits.')