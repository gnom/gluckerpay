from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager
from flask_bootstrap import Bootstrap
import logging

# app creation
app = Flask(__name__)


# loading config from file config.py
app.config.from_object('config')


# getting logger object
formatter = logging.Formatter(
            '[%(asctime)s] [%(filename)s:%(lineno)d] [ %(levelname)8s ] - %(message)s')
handler = logging.StreamHandler()
handler.setLevel(logging.DEBUG)
handler.setFormatter(formatter)
log = logging.getLogger('werkzeug')
log.setLevel(logging.DEBUG)
log.addHandler(handler)
if app.config['DEBUG']:
    log.warning('Running in DEBUG MODE. Do not run debug mode in production!')


# dumping configuration to log
log.debug(app.config)


# setting up database
db = SQLAlchemy(app)


# setup of LoginManager
login_manager = LoginManager()
login_manager.session_protection = 'strong'
login_manager.login_view = 'login'
login_manager.init_app(app)


# Bootstrap setup
bootstrap = Bootstrap()
bootstrap.init_app(app)



from app import views, models