import os, logging
basedir = os.path.abspath(os.path.dirname(__file__))

SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(basedir, 'app.db')
SQLALCHEMY_MIGRATE_REPO = os.path.join(basedir, 'db_repository')
SQLALCHEMY_TRACK_MODIFICATIONS = True

WTF_CSRF_ENABLED = True
SECRET_KEY = os.urandom(32)

DEBUG = True

LOGGING_FORMAT = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
LOGGING_LEVEL = logging.DEBUG

DECIMAL_SEPARATOR = ','
CURRENCY_SIGN = '€'
CURRENCY_PLACEMENT = 'after'   # can either be before or after
RECHARGE_VALUES = [0.5, 1, 2, 5, 10, 20, 50]

BOOTSTRAP_SERVE_LOCAL=True
BASEDIR = basedir